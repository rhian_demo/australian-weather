# australian-weather
A simple but awesome data science project exploring the weather in Australia. Used in our "Git for Me" training course.

 - Authors: Rhian Davies & Theo Roe (Jumping Rivers)
Mirrored from https://gitlab.com/jumpingrivers-public/australian-weather to https://github.com/jumpingrivers/australian-weather, edit in GitLab only.